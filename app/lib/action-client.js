import { Http } from "@nativescript/core";
const baseUrl = "https://dev-we-run-api.dylanbaine.com";
const url = `${baseUrl}/api/qar`;

export async function singleAction(action, parameters = {}) {
  return await Http.request({
    url,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest"
    },
    content: JSON.stringify({
      action,
      parameters
    })
  }).then(
    response => {
      const result = response.content.toJSON();
      return result;
    },
    e => {
      console.error("action error", action);
      console.error(e);
    }
  );
}

export async function actions(data) {
  return await Http.request({
    url,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest"
    },
    content: JSON.stringify(data)
  }).then(
    response => {
      const result = response.content.toJSON();
      return result;
    },
    e => {
      console.error(e);
    }
  );
}
