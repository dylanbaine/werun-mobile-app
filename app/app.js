import Vue from "nativescript-vue";
import { getCurrentAccount } from "./actions/user";

import Home from "./pages/Home";

Vue.mixin({
  data() {
    return {
      user: {}
    };
  },
  mounted() {
    const { user } = getCurrentAccount();
    if (user) {
      this.user = user;
    }
    this.$root.$on("setUser", user => {
      this.user = user;
    });
  },
  methods: {
    loadingState() {
      this.$root.$emit("setState", "loading");
    },
    clearLoadingState() {
      this.$root.$emit("setState", null);
    },
    setUser(user) {
      this.$root.$emit("setUser", user);
    }
  }
});

new Vue({
  data: {
    globalUserState: null
  },
  render: h => h("frame", [h(Home)])
}).$start();
