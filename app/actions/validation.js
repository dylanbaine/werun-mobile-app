const ValidationResponse = {
  valid: false,
  message: ""
};
/**
 * @param {ValidationResponse} message
 * @returns
 */
function respondWithValidationMessage(message = false) {
  return {
    valid: message === false,
    message
  };
}

export function validateEmail(email) {
  let message = false;
  if (!email || !email.length) {
    message = "Email is required";
  } else if (!email.includes("@")) {
    message = "Email is invalid";
  }
  return respondWithValidationMessage(message);
}

export function validatePassword(password) {
  let message = false;
  if (!password) {
    message = "A password is required";
  } else if (password.length < 6) {
    message = "Your password must be atleast 6 characters long";
  }
  return respondWithValidationMessage(message);
}
