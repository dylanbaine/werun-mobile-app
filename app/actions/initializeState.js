import { actions } from "~/lib/action-client";
import { getCurrentAccount } from "./user";

export default async function initializeState(message) {
  const { status } = await actions({
    status: {
      action: "CheckHealth",
      parameters: {
        message
      }
    }
  });

  const currentUser = getCurrentAccount();

  return {
    message: status.message,
    currentUser
  };
}
