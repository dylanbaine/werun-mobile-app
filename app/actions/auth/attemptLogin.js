import { singleAction } from "~/lib/action-client";
import { setCurrentAccount, setLastUsedEmailOnDevice } from "../user";

export default async function attemptLogin(email, password) {
  const response = await singleAction("Users/Login", { email, password });
  if (response.success) {
    setCurrentAccount(response);
    setLastUsedEmailOnDevice(response.user.email);
    return response.user;
  }
  return false;
}
