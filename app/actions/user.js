import {
  getString,
  hasKey,
  setString
} from "@nativescript/core/application-settings";

const key = "current-account";

export function getCurrentAccount() {
  if (hasKey(key)) {
    const account = getString(key);
    return JSON.parse(account);
  }
  return false;
}

export function setCurrentAccount(account) {
  setString(key, JSON.stringify(account));
}

export function setLastUsedEmailOnDevice(email) {
  setString(`${key}-last-used-email`, email);
}
export function getLastUsedEmailOnDevice() {
  return getString(`${key}-last-used-email`, null);
}
